// Copyright (c) 2014-2019, The Monero Project
//
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without modification, are
// permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice, this list of
//    conditions and the following disclaimer.
//
// 2. Redistributions in binary form must reproduce the above copyright notice, this list
//    of conditions and the following disclaimer in the documentation and/or other
//    materials provided with the distribution.
//
// 3. Neither the name of the copyright holder nor the names of its contributors may be
//    used to endorse or promote products derived from this software without specific
//    prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
// THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF
// THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// Parts of this file are originally copyright (c) 2012-2013 The Cryptonote developers

#include "checkpoints.h"

#include "common/dns_utils.h"
#include "string_tools.h"
#include "storages/portable_storage_template_helper.h" // epee json include
#include "serialization/keyvalue_serialization.h"
#include <vector>

using namespace epee;

#undef MONERO_DEFAULT_LOG_CATEGORY
#define MONERO_DEFAULT_LOG_CATEGORY "checkpoints"

namespace cryptonote
{
  /**
   * @brief struct for loading a checkpoint from json
   */
  struct t_hashline
  {
    uint64_t height; //!< the height of the checkpoint
    std::string hash; //!< the hash for the checkpoint
        BEGIN_KV_SERIALIZE_MAP()
          KV_SERIALIZE(height)
          KV_SERIALIZE(hash)
        END_KV_SERIALIZE_MAP()
  };

  /**
   * @brief struct for loading many checkpoints from json
   */
  struct t_hash_json {
    std::vector<t_hashline> hashlines; //!< the checkpoint lines from the file
        BEGIN_KV_SERIALIZE_MAP()
          KV_SERIALIZE(hashlines)
        END_KV_SERIALIZE_MAP()
  };

  //---------------------------------------------------------------------------
  checkpoints::checkpoints()
  {
  }
  //---------------------------------------------------------------------------
  bool checkpoints::add_checkpoint(uint64_t height, const std::string& hash_str)
  {
    crypto::hash h = crypto::null_hash;
    bool r = epee::string_tools::hex_to_pod(hash_str, h);
    CHECK_AND_ASSERT_MES(r, false, "Failed to parse checkpoint hash string into binary representation!");

    // return false if adding at a height we already have AND the hash is different
    if (m_points.count(height))
    {
      CHECK_AND_ASSERT_MES(h == m_points[height], false, "Checkpoint at given height already exists, and hash for new checkpoint was different!");
    }
    m_points[height] = h;
    return true;
  }
  //---------------------------------------------------------------------------
  bool checkpoints::is_in_checkpoint_zone(uint64_t height) const
  {
    return !m_points.empty() && (height <= (--m_points.end())->first);
  }
  //---------------------------------------------------------------------------
  bool checkpoints::check_block(uint64_t height, const crypto::hash& h, bool& is_a_checkpoint) const
  {
    auto it = m_points.find(height);
    is_a_checkpoint = it != m_points.end();
    if(!is_a_checkpoint)
      return true;

    if(it->second == h)
    {
      MINFO("CHECKPOINT PASSED FOR HEIGHT " << height << " " << h);
      return true;
    }else
    {
      MWARNING("CHECKPOINT FAILED FOR HEIGHT " << height << ". EXPECTED HASH: " << it->second << ", FETCHED HASH: " << h);
      return false;
    }
  }
  //---------------------------------------------------------------------------
  bool checkpoints::check_block(uint64_t height, const crypto::hash& h) const
  {
    bool ignored;
    return check_block(height, h, ignored);
  }
  //---------------------------------------------------------------------------
  //FIXME: is this the desired behavior?
  bool checkpoints::is_alternative_block_allowed(uint64_t blockchain_height, uint64_t block_height) const
  {
    if (0 == block_height)
      return false;

    auto it = m_points.upper_bound(blockchain_height);
    // Is blockchain_height before the first checkpoint?
    if (it == m_points.begin())
      return true;

    --it;
    uint64_t checkpoint_height = it->first;
    return checkpoint_height < block_height;
  }
  //---------------------------------------------------------------------------
  uint64_t checkpoints::get_max_height() const
  {
    std::map< uint64_t, crypto::hash >::const_iterator highest = 
        std::max_element( m_points.begin(), m_points.end(),
                         ( boost::bind(&std::map< uint64_t, crypto::hash >::value_type::first, _1) < 
                           boost::bind(&std::map< uint64_t, crypto::hash >::value_type::first, _2 ) ) );
    return highest->first;
  }
  //---------------------------------------------------------------------------
  const std::map<uint64_t, crypto::hash>& checkpoints::get_points() const
  {
    return m_points;
  }

  bool checkpoints::check_for_conflicts(const checkpoints& other) const
  {
    for (auto& pt : other.get_points())
    {
      if (m_points.count(pt.first))
      {
        CHECK_AND_ASSERT_MES(pt.second == m_points.at(pt.first), false, "Checkpoint at given height already exists, and hash for new checkpoint was different!");
      }
    }
    return true;
  }

  bool checkpoints::init_default_checkpoints(network_type nettype)
  {
    if (nettype == TESTNET)
    {
      return true;
    }
    if (nettype == STAGENET)
    {
      return true;
    }
    ADD_CHECKPOINT(1, "4e715965b2fc3a24bdf2c267a64b59d28195f39a4a3935001fbc93f7c01a3065");
    ADD_CHECKPOINT(1000, "90ce6d5e04394aaab5f9b03af32f996ea3732f6e1697410f988adb3d5d61b698");
    ADD_CHECKPOINT(10000, "e08bb1f70c0cf6a66423065d892b871617ef5c9ce3de0eedd9a7701809ef097c");
    ADD_CHECKPOINT(20000, "37e21fc0ecc11ae4e010d9abe1101bb092df25431c9a4787afd09d41ef7bdc06");
    ADD_CHECKPOINT(30000, "b0808c575e36c4a02d91c77bbc12ff2ec258726918652cf4ac36cf7c373e8cd0");
    ADD_CHECKPOINT(40000, "1e8e55019fe4fccd2131167d27031cb50fc775bf452cf18eaa7546a0c6ed1e67");
    ADD_CHECKPOINT(50000, "95aaf6aed2528b7026fec58da8d62b406d5351234764ef3e0c8b52c9832c08ed");
    ADD_CHECKPOINT(60000, "5b940093ebb9256984c370ad068e316cddd8501b0d47bf186f015be299b20e76");
    ADD_CHECKPOINT(70000, "a69a9da7b11532574fa4b500dcbb061b0530cacb4668974eb699e5513c9b6f05");
    ADD_CHECKPOINT(80000, "923daba1e10cc29dc81ab08860c74301c507309a45134232471e841aab25f509");
    ADD_CHECKPOINT(90000, "8f2691799c4821b5f8adda409fd9d0ca9861ad2911392100d95a6cbc0abb21a9");
    ADD_CHECKPOINT(100000, "1835a2bab46d545da5cf13c966cf26d4b53de01ec79f2fc885610e9e8fab8b7f");
    ADD_CHECKPOINT(110000, "00d1b8607c8c69f27ce1fc3d3af44d36bd448f308d77f18c3c875175ea1ff5d9");
    ADD_CHECKPOINT(120000, "0c012bfbace17f4f6830eb36162ca4477e3976950defbbe65804e41a7599478f");
    ADD_CHECKPOINT(130000, "a62b3332e49a2391d68a8b97bebafca7075b1a0402b4e2e408dea40caf957ec7");
    ADD_CHECKPOINT(140000, "68da2d0bdb8f68402f3a01630bc9f7ba60e47f264fd2e02f047e15319f248657");
    ADD_CHECKPOINT(150000, "e0215ceb03fcd1b486addda4855463543aad53ef359e8d71cc149e426984ae4d");
    ADD_CHECKPOINT(160000, "8268c6867e57d497ce0098575e1c7cd851a4c91476b716b0075d7d132b9d353d");
    ADD_CHECKPOINT(170000, "9263b4c77ffe41d3c8860b45fbe6510edd806ecbd45e13eaf6c57ce0f404fb00");
    ADD_CHECKPOINT(180000, "9fc8e0c5faf54e3af32e55a06eb04276ae9ad07cc2b6152ee46b47d6a2ffbe21");
    ADD_CHECKPOINT(190000, "2c80287d409821984f18b6da87804d0bc46c5ee1b52336558405146cabea7910");
    ADD_CHECKPOINT(200000, "ca1f23f5a1a2c3d8bcf3faa17caf50e685dd9c7b24c8829be812272a8b9d6b49");
    ADD_CHECKPOINT(210000, "32eeed5585af728f458570f8cbcfa016432d66263a89b1d11ed6695c42a7edf6");
    ADD_CHECKPOINT(220000, "de138553a0f32ac3adab8136e6ce5647a169ecf7ed4de4a0ad6c41ad6ed9dfa7");
    ADD_CHECKPOINT(225000, "32eb4ef1720fdb929c0ba80d03c8bb2ebd89f76ab1f94c9c1259762834756a6c");
    ADD_CHECKPOINT(226000, "6f9d69d7b16f2ba9192c67247f1d35801ff0982fb3af486d7b85105fb5fb885a");
    ADD_CHECKPOINT(227000, "9be8267f23ff8ca80ae3e41ed80023dc98c6e8dff5bb026df64b7733e33e8c16"); // begin v15 DAA hell
    ADD_CHECKPOINT(228000, "d8744ccc0d80fdd8f8e4475e753bfc75b8cdf122f8e2ff903f9d6b17dd253b35");
    ADD_CHECKPOINT(229000, "9143e3805bdcfc559d013f3a626ca0db2ac0179cd8790672bd1dd2b6736ddee7");
    ADD_CHECKPOINT(230000, "ef490f3bf7195fa8715a95da8e69ab63e08449145cbd4b666074644487843e85");
    ADD_CHECKPOINT(231000, "e73bec69905f157976ce1343e5ae66c5d77135faaa0625f295ad8453bbe8ed5f");
    ADD_CHECKPOINT(232000, "fc21b400820f0bb331b054535c396a281c5692948d5e8427e000b6ec404be3bb");
    ADD_CHECKPOINT(233000, "c22c8dfcd0838f5d19dd2b78f49f8d99fbc07e60dbec6369c6e855c9df150296");
    ADD_CHECKPOINT(234000, "0dbe8be4aed3036ab9000c53ae76b04e62d9a767fb0e071a27bf1857cea1cb84");
    ADD_CHECKPOINT(235000, "6ce55e630f6ec33b226e4a34223b2edc400e93645ac9ddae33eb45fd67588d21");
    ADD_CHECKPOINT(236000, "30c4b1e7ea95a239ebc676c0137a6b89de5d3154aadaf32801729c26642ff911");
    ADD_CHECKPOINT(237000, "4b8b77aaf9134d8b051344da28cfeab7f07385db8acc1dd0cefa80ccb3e46870");
    ADD_CHECKPOINT(238000, "bee5b38d676f7734dee04b3ad3a08db97c8718ba759051d194caa68917b306d6");
    ADD_CHECKPOINT(239000, "0c69ff39e06ff87059493cc17d3f1aaeec9507d16ab3a2df74c0c6ab6eff8643");
    ADD_CHECKPOINT(240000, "db03d325abaf29ca156e2e684d0eaa425bd38b1eadd2e0a4dc4f25f44b38ccbc");
    ADD_CHECKPOINT(241000, "b8401d3771aa656deddd29e7bb8081b76b3be77494d33375f8352af9798b7f8d");
    ADD_CHECKPOINT(242000, "01c5e6330a35915ca686624f8c8435b6ffa06d97a147b314041cae54bc0035da");
    ADD_CHECKPOINT(243000, "6d7b04853bb25d0878436caeaaf0c35e713512ec77e8a97c7c0e59524569ce09");
    ADD_CHECKPOINT(244000, "60ac3f24202d32ce0b172eedc1c0a738cc3265a0bf0b490c1ec2d9e33c29bd32");
    ADD_CHECKPOINT(245000, "1c0e12266bab93b717c974421d03eb9c6e1280162f2301d95c910cfc2bb5ca5b");
    ADD_CHECKPOINT(246000, "55c0fad490d7420eeb47db7cfa7376005a343a1a8c2d8e8181547a614efa805f");
    ADD_CHECKPOINT(247000, "ac2ba83d11fc935ed38079ecda20841f288f62ed3e934d60aed81b325963b571");
    ADD_CHECKPOINT(248000, "cce99ed3f3a16b42c9bb6fe3f67b3c95eced8e4f126d3db81b1f7f3247fc05c5");
    ADD_CHECKPOINT(249000, "7b56720a201949273e4e7d2fc4ad6b77ad44d8fbd8331ea71b86e659a8916711");
    ADD_CHECKPOINT(250000, "ca26e951c8f81550bcd6a19fb2a401c0913bf6e592f63568d2baa211f579ec5f");
    ADD_CHECKPOINT(251000, "d7d056ccbf23db214ab34e29a5ad19d9f452b2a21da7d6026bfc475190865031");
    ADD_CHECKPOINT(252000, "777d90852cc0c5a5a27cec3185d6ed4c2fb1cb9948addeeb51c5510cdadaae8b");
    ADD_CHECKPOINT(253000, "c250757cc7f22de3dfdd9d621b2c1474c32aca36d3b652578307acf990f114fd");
    ADD_CHECKPOINT(254000, "fe531db2e8d4df00d4f11c7a0fbda81533431871bffc14683183eef9cb48bd6f");
    ADD_CHECKPOINT(255000, "f29cfa8b051a688480033819e4a37a59fdee58823f65f436d0a9c14e32487e85");
    ADD_CHECKPOINT(256000, "e4e103a3352d00e9a2357bea7992cd1fc15c84c9cca13d6325ab0ec0deeb02a0");
    ADD_CHECKPOINT(257000, "50b4467e493bc893c52b8b9275ccf24be7a38605458b1dfa568babb16d46e4a8");
    ADD_CHECKPOINT(258000, "ef7dbb3509301be5c9a996578af58bc086062f4dd35ee368ef87608a25ab9a93");
    ADD_CHECKPOINT(259000, "c98ac20dec6253a369f205474e0cea686785aee6a88e1141e80fac6b5fd47842");
    ADD_CHECKPOINT(260000, "f6b2a7d2931b32a0f99f32637893850a003b66d8f4756baea0313eaeb416b989");
    ADD_CHECKPOINT(261000, "53c10428d90497f6e3a56a8d0ef25c3d49be5fe7a89b716113f1595244409efd");
    ADD_CHECKPOINT(262000, "b89836cc8c564891b234612660ef44273c6d761cc44e96b6c52e422d72ca2ff2");
    ADD_CHECKPOINT(263000, "3cbaa0d05768752b3ff03e8e9595d92789b37d56ef1747cc9ed044b660de2227");
    ADD_CHECKPOINT(264000, "d09770095a68edd7302ccfeab30eed49029479f5e73548bda5cd63619c855778");
    ADD_CHECKPOINT(265000, "e305652ee08c6cd59850237d7a6367ced455b277e5c6763a49c7f7f5b8a343e1"); 
    ADD_CHECKPOINT(266000, "72fc69b60e983eeb479a2932a95317a9b7bcd6a8d55994bb9f015d01fbcc7772"); 
    ADD_CHECKPOINT(267000, "28107b8bfc95d26f3c0575213ccdb7ab8daecebd51093d41d154df71a4e6366b");
    ADD_CHECKPOINT(268000, "19e5479c4d57cacb313dfaf37193a1e9b787c565e67f660735bf21c24bc8701e"); 
    ADD_CHECKPOINT(269000, "a4e3a7402f4358c054b96ffd9ba4c6349c42849d4a4bb52a7237068bb705fd27");
    ADD_CHECKPOINT(270000, "078f1d3d5d8b9ef2e4297e7c87baa34e6a89ce69aeed0a1043803267d5d2a458");
    ADD_CHECKPOINT(271000, "6b5d2b958201b4fb9ca286204746ba1406188da710337079be82c5de1c04a173");
    ADD_CHECKPOINT(272000, "9bd4eff854577d0991839cfe0abf9e17a799262dc3e82ab78b70f363572996ea");
    ADD_CHECKPOINT(273000, "eb0f66b700bdd63b300a4cd2ccbbedad984d898a6ce27e3257fb1410f52e40cb");
    ADD_CHECKPOINT(274000, "4f456efea6c0a12d21790426b9ba8c94146cea15b1a8108e4e969723f8f8bec6");
    ADD_CHECKPOINT(275000, "3909ef4b0626404d0c068f07e83aa40a11c3cf34b0b433c927916587f1ede013");
    ADD_CHECKPOINT(276000, "be243cbb102a74bdae12a264653614752eb97f55116a45a706f6559cdf5cbdfb");
    ADD_CHECKPOINT(277000, "8ed18e7f10b301cdd7663e7a75fcf70b3bc9c7375fea7c49e6589ca9bee6fa76"); // end v15
    ADD_CHECKPOINT(280000, "6e218aa8d2adffd71db206a35759027a60be4b5fae66e83eeb4fff3b63a6c0e2");
    ADD_CHECKPOINT(290000, "902ea9757fe8a851edbd663f632dba7dde29c4440f8bdc4cb2636743031125c0");
    ADD_CHECKPOINT(300000, "78eb0da11e4bfabf0c8a1af6b59e1799acec2c34edda393a06ee99cec08e457c");
    ADD_CHECKPOINT(310000, "d661ef54a233439252b29a84a29dc546414528b599323e9c22e2a960517c9554");
    ADD_CHECKPOINT(320000, "a3938b7ae51e40fa5784b13d9c3559ea273e9cd8ac45b79ad85870e9911f5137");
    ADD_CHECKPOINT(330000, "765ccb3e3708d3659f3acb3b08db4158415c12660c1b2a0d4aeee6aef2bc5320");
    ADD_CHECKPOINT(340000, "454fdb1b85a8dcc58f337c5f169a9f23a2c226c3f32576c7e3bbb12bba95e9d4");
    ADD_CHECKPOINT(350000, "edb8e0793dfe64e5795ccd09f55b7dca5f8ddc363da02e0cb0bd9211a2150685");
    ADD_CHECKPOINT(360000, "8837962c3d4704851ea72eaa7a0e0d30da9ae9967fc54aaa8e2efe208b97951d");
    ADD_CHECKPOINT(370000, "7248a92bd36234dc01ab632ed8628e457a0722df99f218620453793c10a038e1");
    ADD_CHECKPOINT(380000, "ecd5772abec412d2e0775d37e58a24dd65264161edc57df51ccd49ceec90e29f");

    return true;
  }

  bool checkpoints::load_checkpoints_from_json(const std::string &json_hashfile_fullpath)
  {
    boost::system::error_code errcode;
    if (! (boost::filesystem::exists(json_hashfile_fullpath, errcode)))
    {
      LOG_PRINT_L1("Blockchain checkpoints file not found");
      return true;
    }

    LOG_PRINT_L1("Adding checkpoints from blockchain hashfile");

    uint64_t prev_max_height = get_max_height();
    LOG_PRINT_L1("Hard-coded max checkpoint height is " << prev_max_height);
    t_hash_json hashes;
    if (!epee::serialization::load_t_from_json_file(hashes, json_hashfile_fullpath))
    {
      MERROR("Error loading checkpoints from " << json_hashfile_fullpath);
      return false;
    }
    for (std::vector<t_hashline>::const_iterator it = hashes.hashlines.begin(); it != hashes.hashlines.end(); )
    {
      uint64_t height;
      height = it->height;
      if (height <= prev_max_height) {
	LOG_PRINT_L1("ignoring checkpoint height " << height);
      } else {
	std::string blockhash = it->hash;
	LOG_PRINT_L1("Adding checkpoint height " << height << ", hash=" << blockhash);
	ADD_CHECKPOINT(height, blockhash);
      }
      ++it;
    }

    return true;
  }

  bool checkpoints::load_checkpoints_from_dns(network_type nettype)
  {
    std::vector<std::string> records;

    // All four MoneroPulse domains have DNSSEC on and valid
    static const std::vector<std::string> dns_urls = {};

    static const std::vector<std::string> testnet_dns_urls = {};

    static const std::vector<std::string> stagenet_dns_urls = {};

    if (!tools::dns_utils::load_txt_records_from_dns(records, nettype == TESTNET ? testnet_dns_urls : nettype == STAGENET ? stagenet_dns_urls : dns_urls))
      return true; // why true ?

    for (const auto& record : records)
    {
      auto pos = record.find(":");
      if (pos != std::string::npos)
      {
        uint64_t height;
        crypto::hash hash;

        // parse the first part as uint64_t,
        // if this fails move on to the next record
        std::stringstream ss(record.substr(0, pos));
        if (!(ss >> height))
        {
    continue;
        }

        // parse the second part as crypto::hash,
        // if this fails move on to the next record
        std::string hashStr = record.substr(pos + 1);
        if (!epee::string_tools::hex_to_pod(hashStr, hash))
        {
    continue;
        }

        ADD_CHECKPOINT(height, hashStr);
      }
    }
    return true;
  }

  bool checkpoints::load_new_checkpoints(const std::string &json_hashfile_fullpath, network_type nettype, bool dns)
  {
    bool result;

    result = load_checkpoints_from_json(json_hashfile_fullpath);
    if (dns)
    {
      result &= load_checkpoints_from_dns(nettype);
    }

    return result;
  }
}
